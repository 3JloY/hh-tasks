package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String... args) {

        Converter converter = new Converter();

        List<int[]> dataList = getData(args);

        for (int[] data : dataList) {
            try {
                System.out.println(converter.divide(data[0], data[1], data[2]));
            } catch (IllegalArgumentException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private static List<int[]> getData(String... args) {
        List<int[]> result = new ArrayList<>();
        List<String[]> strings = new ArrayList<>();
        try {
            strings.add(getFromTerminal());
        } catch (IOException | IllegalArgumentException e) {
            System.err.println(e.getMessage());
        }
        for (String[] strArr : strings) {
            try {
                int a = Integer.parseInt(strArr[0]);
                int b = Integer.parseInt(strArr[1]);
                int k = Integer.parseInt(strArr[2]);
                result.add(new int[]{a, b, k});
            } catch (NumberFormatException e) {
                System.err.println("Error parsing integers from data: " +
                        Arrays.toString(strArr));
            }
        }
        return result;
    }

    private static String[] getFromTerminal() throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String[] data = new String[3];
            System.out.println("Enter data:");
            System.out.print("a = ");
            data[0] = br.readLine();
            System.out.print("b = ");
            data[1] = br.readLine();
            System.out.print("k = ");
            data[2] = br.readLine();
            return data;
        }
    }
}
