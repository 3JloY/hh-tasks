package com.company;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public String divide(int dividend, int divisor, int scale) {
        if (divisor == 0) {
            throw new IllegalArgumentException("Error: The second argument (divisor) " +
                    "can not be equal to 0 (division by 0)");
        }
        if (scale <= 0) {
            throw new IllegalArgumentException("Error: The third argument (scale) " +
                    "must be greater than 0 (division by 0)");
        }

        String sign = dividend * divisor < 0 ? "-" : "";
        dividend = Math.abs(dividend);
        divisor = Math.abs(divisor);
        return sign + intConverter(dividend / divisor, scale) +
                floatConvert(dividend, divisor, scale);
    }

    String intConverter(int number, int scale) {
        StringBuilder result = new StringBuilder();
        while (number >= scale) {
            result.insert(0, number % scale);
            number /= scale;
        }
        result.insert(0, number);
        return result.toString();
    }

    String floatConvert(int dividend, int divisor, int scale) {

        List<Integer> remainders = new ArrayList<>();

        int number = dividend % divisor;

        int count = 0;
        String result = "";

        while (number != 0 && count < divisor) {
            result += number * scale / divisor;
            remainders.add(number);
            number = number * scale % divisor ;
            count++;
        }

        int start = 0;
        int end = 0;
        boolean found = false;
        search:
        while (start < remainders.size() - 1) {
            for (int i = start + 1; i < remainders.size(); i++) {

                if (remainders.get(start).intValue() == remainders.get(i).intValue()) {
                    found = true;
                    end = i;
                    break search;
                }
            }
            start++;
        }

        if (found) {
            result = result.substring(0, start) +
                    "(" + result.substring(start, end) + ")";
        }

        return result.equals("") ? result : "." + result;
    }
}