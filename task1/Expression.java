package com.poly;

import java.util.Iterator;
import java.util.NavigableMap;
import java.util.TreeMap;


public class Expression {

    private String var;
    private NavigableMap<Integer, Integer> part_poly; //<степень, коэффициент>

    /**
     * @param var Переменная полинома
     * */
    public Expression(String var) {
        this.var = var;
        this.part_poly = new TreeMap<>();
    }

    /**
     * Добавляет к этому полиному части полином с указанными степенью и коэффициентом.
     * Если полином уже содержит части полином указанной степени, коэффициенты части полиномов
     * складываются (с учетом знака).
     *
     * @param power Степень части полинома
     * @param coefficient Коэффициент части полинома
     *
     * @return Этот полином
     *
     * @see #add(Expression)
     * */
    public Expression add(int power, int coefficient) {
        if (part_poly.containsKey(power)) {
            part_poly.put(power, part_poly.get(power) + coefficient);
        } else {
            part_poly.put(power, coefficient);
        }
        return this;
    }

    /**
     * Добавляет к этому полиному другой полином.
     * Сложение коэффициентов при степенях полиномов происходит как описано в {@link #add(int, int)}.
     *
     * @param expression Добавляемый полином
     *
     * @return Этот полином
     *
     * @see #add(int, int)
     * */
    public Expression add(Expression expression) {
        for (Integer power : expression.part_poly.keySet()) {
            this.add(power, expression.part_poly.get(power));
        }
        return this;
    }

    /**
     * Произведение двух полиномов. Степени частей полиномов складываются, а их коэффициенты перемножаются.
     *
     * @param p1 Первый полином
     * @param p2 Второй полином
     *
     * @return Произведение двух полиномов
     *
     * @see #power(Expression, int)
     * */
    public static Expression multiply(Expression p1, Expression p2) {
        Expression result = new Expression(p1.var);
        for (Integer p1Power : p1.part_poly.keySet()) {
            for (Integer p2Power : p2.part_poly.keySet()) {
                result.add(
                        p1Power + p2Power,
                        p1.part_poly.get(p1Power) * p2.part_poly.get(p2Power)
                );
            }
        }
        return result;
    }

    /**
     * Возведение полинома в степень.
     * Мономы обрабатываются как описано в {@link #multiply(Expression, Expression)}.
     *
     * @param expression Полином
     * @param power Степень
     *
     * @return Полином в указанной степени
     *
     * @see #multiply(Expression, Expression)
     * */
    public static Expression power(Expression expression, int power) {
        return power == 0
                ? new Expression(expression.var).add(0, 1)
                : power == 1
                ? expression
                : multiply(expression, power(expression, --power));
    }

    private void removeZeros() {
        Iterator<Integer> iterator = part_poly.keySet().iterator();
        while (iterator.hasNext()) {
            if (part_poly.get(iterator.next()) == 0) iterator.remove();
        }
    }

    /**
     * Текстовое представление полинома в развернутом виде.
     *
     * @return Текстовое представление полинома в развернутом виде
     * */
    @Override
    public String toString() {

        removeZeros();
        if (part_poly.isEmpty()) return "";

        StringBuilder sb = new StringBuilder();

        //степень полинома
        int sortPower = part_poly.lastKey();
        for (Integer power : part_poly.descendingKeySet()) {
            int coefficient = part_poly.get(power);

            boolean firstMonomial = power == sortPower;
            if (firstMonomial) {
                sb.append(coefficient > 0 ? "" : "-");
            } else {
                sb.append(coefficient > 0 ? " + " : " - ");
            }

            int absCoefficient = Math.abs(coefficient);
            if (power == 0) {
                sb.append(absCoefficient);
            } else if (power == 1 && absCoefficient == 1) {
                sb.append(var);
            } else if (power == 1) {
                sb.append(absCoefficient).append(var);
            } else if (absCoefficient == 1) {
                sb.append(var).append("^").append(power);
            } else {
                sb.append(absCoefficient).append(var).append("^").append(power);
            }
        }

        return sb.toString();
    }
}
