package com.poly;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String... args) {

        //получаем входные данные
        List<String> dataList = getPolynom(args);

        //разворачиваем выражение
        for (String data : dataList) {
            System.out.println(new PolyReader(data).read());
        }
    }

    private static List<String> getPolynom(String... args) {
        List<String> result = new ArrayList<>();
        try {
            result.add(getData());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return result;
    }

    private static String getData() throws IOException {
        try (BufferedReader data = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.print("Enter polynom: ");
            return data.readLine();
        }
    }
}
