package com.poly;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

public class PolyReader {

    private enum Operators {
        SUB_SUM,           //сложение и вычитание части полинома
        ADD_PARENTHESIS,        //добавление скобок
        SUB_PARENTHESIS,   //вычитание скобок
        MUL,               //умножение
        MULTM,             //умножение на части полином
        MULT_PARENTHESIS,   //умножение скобки на части полином
        POWER,                  //возведение в степень
        OPEN_PARENTHESIS,       //открывающая скобка
        CLOSE_PARENTHESIS       //закрывающая скобка
    }

    private static final String POLYNOMIAL_PATTERN = "[-+]?[0-9]*[A-Za-z]?\\^?[0-9]*";
    private String source;
    private String variable;
    private int charPointer;

    /**
     * @param source Выражение для разбора
     * */
    public PolyReader(String source) {
        spellCheck(source);
        charPointer = 0;
    }

    /**
     * Возвращает результат разбора выражения в виде полинома.
     *
     * @return Полином
     * */
    public Expression read() {

        return source == null || source.length() == 0
                ? null
                : readOperators();
    }

    /**
     * Парсит выражение по операциям.
     *
     * @return Полином
     *
     * @throws IllegalStateException Если выражение содержит некорректные символы или операции
     * */
    Expression readOperators() {

        Deque<Expression> tempResult = new ArrayDeque<>();

        loop:
        while (charPointer < source.length()) {

            char symbol = source.charAt(charPointer);
            char nextSymbol = charPointer < source.length() - 1
                    ? source.charAt(charPointer + 1)
                    : 0;
            Operators op;

            if (symbol == '+' || symbol == '-' || Character.isLetterOrDigit(symbol)) {
                if (symbol == '+' && nextSymbol == '(') {
                    op = Operators.ADD_PARENTHESIS;
                } else if (symbol == '-' && nextSymbol == '(') {
                    op = Operators.SUB_PARENTHESIS;
                } else {
                    op = Operators.SUB_SUM;
                }
            } else if (symbol == '(') {
                if (tempResult.isEmpty()) {
                    op = Operators.OPEN_PARENTHESIS;
                } else {
                    op = Operators.MUL;
                }
            } else if (symbol == ')') {
                if (nextSymbol == '^') {
                    op = Operators.POWER;
                } else if (Character.isLetterOrDigit(nextSymbol)) {
                    op = Operators.MULT_PARENTHESIS;
                } else {
                    op = Operators.CLOSE_PARENTHESIS;
                }
            } else if (symbol == '*') {
                if (nextSymbol == '(') {
                    charPointer++;
                    op = Operators.MUL;
                } else {
                    op = Operators.MULTM;
                }
            } else throw new IllegalStateException("Fail on symbol: " + symbol);

            switch (op) {
                case SUB_SUM:
                    String monomial = new Scanner(source.substring(charPointer))
                            .findInLine(POLYNOMIAL_PATTERN);
                    charPointer += monomial.length();
                    tempResult.add(parseMonomial(monomial));
                    break;
                case ADD_PARENTHESIS:
                    charPointer += 2;
                    tempResult.add(readOperators());
                    break;
                case SUB_PARENTHESIS:
                    charPointer += 2;
                    tempResult.add(Expression
                            .multiply(new Expression(variable).add(0, -1), readOperators()));
                    break;
                case OPEN_PARENTHESIS:
                    charPointer++;
                    tempResult.add(readOperators());
                    break;
                case CLOSE_PARENTHESIS:
                    charPointer++;
                    break loop;
                case MUL:
                case MULTM:
                    charPointer++;
                    if (tempResult.isEmpty()) {
                        throw new IllegalStateException("Operations stack is empty on symbol: " + symbol);
                    }
                    Expression operand1 = tempResult.pollLast();
                    Expression operand2;
                    if (op == Operators.MUL) {
                        operand2 = readOperators();
                    } else {
                        monomial = new Scanner(source.substring(charPointer))
                                .findInLine(POLYNOMIAL_PATTERN);
                        charPointer += monomial.length();
                        operand2 = parseMonomial(monomial);
                    }
                    tempResult.add(Expression.multiply(operand1, operand2));
                    break;
                case MULT_PARENTHESIS:
                    charPointer++;
                    operand1 = new Expression(variable);
                    for (Expression expression : tempResult) {
                        operand1.add(expression);
                    }
                    monomial = new Scanner(source.substring(charPointer))
                            .findInLine(POLYNOMIAL_PATTERN);
                    charPointer += monomial.length();
                    operand2 = parseMonomial(monomial);
                    return Expression.multiply(operand1, operand2);
                case POWER:
                    charPointer += 2;
                    String power = new Scanner(source.substring(charPointer))
                            .findInLine("[-+]?[0-9]*");
                    charPointer += power.length();
                    Expression operand = new Expression(variable);
                    for (Expression expression : tempResult) {
                        operand.add(expression);
                    }
                    return Expression.power(operand, Integer.parseInt(power));
            }
        }

        Expression result = new Expression(variable);
        for (Expression expression : tempResult) {
            result.add(expression);
        }

        return result;
    }

    /**
     * Парсит части полином из строки.
     *
     * @param monomial Строка для разбора
     *
     * @return Полином
     *
     * @throws IllegalArgumentException Если строка не является корректной записью части полинома
     * */
    Expression parseMonomial(String monomial) {
        Expression result = new Expression(variable);
        int power;
        int coefficient;
        if (monomial.matches("[-+]?[0-9]*")) {
            power = 0;
            coefficient = new Scanner(monomial).nextInt();
        } else if (monomial.matches("[-+]?" + variable)) {
            power = 1;
            coefficient = monomial.matches("-" + variable) ? -1 : 1;
        } else if (monomial.matches("[-+]?[0-9]*" + variable)) {
            power = 1;
            coefficient = new Scanner(monomial).useDelimiter(variable).nextInt();
        } else if (monomial.matches("[-+]?" + variable + "\\^[0-9]*")) {
            power = new Scanner(monomial).useDelimiter("[-+]?" + variable + "\\^").nextInt();
            coefficient = monomial.matches("-.*") ? -1 : 1;
        } else if (monomial.matches("[-+]?[0-9]*" + variable + "\\^[0-9]*")) {
            Scanner scanner = new Scanner(monomial).useDelimiter(variable + "\\^");
            coefficient = scanner.nextInt();    //������� �����������
            power = scanner.nextInt();          //����� �������
        } else {
            throw new IllegalArgumentException("Not a monomial: " + monomial);
        }
        result.add(power, coefficient);
        return result;
    }

    /**
     * Проверяет корректность выражения и парсит из него переменную.
     *
     * @param rawSource Выражение для проверки
     *
     * @throws IllegalArgumentException Если выражение некорректно
     * */
    void spellCheck(String rawSource) {

        source = rawSource.replaceAll("[^A-Za-z0-9+-^()*]", "");
        if (source.equals("")) return;

        String vars = source.replaceAll("[^A-Za-z]", "");
        String firstVar;
        if (vars.length() > 0
                && vars.matches((firstVar = String.valueOf(vars.charAt(0))) + "+")) {
            variable = firstVar;
        } else {
            throw new IllegalArgumentException("Syntax error: Statement must contain exactly one variable");
        }

        int count = source.replaceAll("[^(]", "").length() - source.replaceAll("[^)]", "").length();
        if (count != 0) {
            throw new IllegalArgumentException(
                    String.format("Syntax error: Missing %d %s parenthes%s '%s'",
                            Math.abs(count),
                            count > 0 ? "closing" : "opening",
                            Math.abs(count) == 1 ? "is" : "es",
                            count > 0 ? ")" : "("
                    )
            );
        }
    }
}
